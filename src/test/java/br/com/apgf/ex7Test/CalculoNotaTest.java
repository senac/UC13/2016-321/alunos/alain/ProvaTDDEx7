
package br.com.apgf.ex7Test;

import br.com.apgf.ex7.Aluno;
import br.com.apgf.ex7.CalculoNota;
import org.junit.Test;
import static org.junit.Assert.*;
public class CalculoNotaTest {
    
    @Test
    public void deveApresentarANotaDoAlunoAprovado(){
        Aluno aluno = new Aluno("AlainPatrick", 8);
        CalculoNota calculo = new CalculoNota();
        String resultado = calculo.Avaliador(aluno);
        assertEquals(calculo.APROVADO, resultado);
    }
   
    
    @Test
    public void deveApresentarANotaDoAlunoRecuperacao(){
        Aluno aluno = new Aluno ("AlainPatrick", 5);
        CalculoNota calculo = new CalculoNota();
        String resultado = calculo.Avaliador(aluno);
        assertEquals(calculo.RECUPERACAO, resultado);
    }
    
    @Test
    public void deveApresentarANotaDoAlunoReprovado(){
        Aluno aluno = new Aluno ("AlainPatrick", 3);
        CalculoNota calculo = new CalculoNota();
        String resultado = calculo.Avaliador(aluno);
        assertEquals(calculo.REPROVADO, resultado);
        
   }


}