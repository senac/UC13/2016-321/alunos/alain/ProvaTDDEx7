
package br.com.apgf.ex7;

public class CalculoNota {
    
    public static final String APROVADO = "Aprovado";
    public static final String RECUPERACAO = "Recuperacao";
    public static final String REPROVADO = "Reprovado";
    
    public String Avaliador(Aluno a){
        if(a.getNota() >= 7){
            return APROVADO;
        }else if (a.getNota() <=6 && a.getNota() >=4){
            return RECUPERACAO;            
        }else{
            return REPROVADO;
        }        
    }
    
}
