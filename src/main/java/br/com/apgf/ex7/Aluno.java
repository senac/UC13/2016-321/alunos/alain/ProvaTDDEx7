
package br.com.apgf.ex7;

public class Aluno {
    
    private String Aluno;
    private double Nota; 

    public Aluno() {
    }
    
    public Aluno(String Aluno, double Nota) {
        this.Aluno = Aluno;
        this.Nota = Nota;
    }
    
    public String getAluno() {
        return Aluno;
    }

    public void setAluno(String Aluno) {
        this.Aluno = Aluno;
    }

    public double getNota() {
        return Nota;
    }

    public void setNota(double Nota) {
        this.Nota = Nota;
    }  
    
}
